import os
import time
import logging
from PIL import Image
import glob
Image.LOAD_TRUNCATED_IMAGES = True



logging.basicConfig(filename='jpeg-to-webp-avif-converter.log', encoding='utf-8', level=logging.DEBUG)
logging.debug('start')

# Path to the current script file
dir_script_path = os.path.dirname(os.path.realpath(__file__))
# Path to the inputs folder
dir_inputs_path = f'{dir_script_path}/inputs/'
# Path to the outputs folder
dir_outputs_path = f'{dir_script_path}/outputs/'
# Path to the backups folder
dir_backups_path = f'{dir_script_path}/backups/'
# Delete every file in the backups folder older than 30 days
# 1 day = 60*60*24 = 86400 seconds
delete_backups_older = 30*86400

def convert_all_once():
    # List for .mp4 files found in the inputs folder
    files_in_dir = []
    current_time = time.time()
    # Iterate through files in the inputs folder
    logging.debug(f"Searching files in {dir_inputs_path}")
    for file_name in os.listdir(dir_inputs_path):
        # If the current path in file_name is a file
        if os.path.isfile(os.path.join(dir_inputs_path, file_name)):
            # If this file_name hase .mp4 extension 
            if file_name.endswith(".jpg") or file_name.endswith(".jpeg"):
                # Add this file_name to files_in_dir list
                logging.debug(f'Found {file_name}')
                files_in_dir.append(file_name)
            else:
                logging.debug(f'Skipping: {file_name}')
    
    # Cycle through all elements in the files_in_dir list
    for file_name in files_in_dir:
        logging.debug(f'Processing: {file_name}')
        # Get current file_name without extension
        file_name_without_extension = file_name.split(".")[:-1][0]
        logging.debug(f'Name without extension: {file_name_without_extension}')
        
        # Use pillow to convert jpeg to webp
        image = Image.open(f'{dir_inputs_path}{file_name}')
        logging.debug('Converting image to webp')
        image = image.resize((1000,1000))
        image = image.convert('RGB')
        image.save(f'{dir_outputs_path}{file_name_without_extension}-1000x1000-product-main.webp', 'webp', optimize = True, quality = 40, method=6)
        # Move converted file_name from the inputs folder to the backups folder
        os.rename(f'{dir_inputs_path}{file_name}',f'{dir_backups_path}{file_name}')

    # Iterate through all files in a backup folder
    for file_name in os.listdir(dir_backups_path):
        # Get current file_name creation time
        file_time = os.stat(f'{dir_backups_path}{file_name}').st_mtime
        # If the file_name creation timestamp is older than the current time - delete_backups_older
        if(file_time < current_time - delete_backups_older): 
            # delete file_name in the backup folder
            os.remove(f'{dir_backups_path}{file_name}')

# Run convertation
def convert():
    try:
        while True:
            convert_all_once()
            time.sleep(5)
    except Exception as err:
        logging.debug(err)

convert()