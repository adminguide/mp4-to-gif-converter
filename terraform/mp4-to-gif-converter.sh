# Author: Sagutdinov Artur, 2023
# Clone repository of MP4 to GIF Converter
git clone https://gitlab.com/adminguide/mp4-to-gif-converter.git

# Move mp4-to-gif-converter folder to /opt/ directory  
sudo mv ./mp4-to-gif-converter/mp4-to-gif-converter/ /opt/
sudo mv ./mp4-to-gif-converter/jpeg-to-webp-avif-converter/ /opt/

# Configure autorestart
sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf

# Update APT cache
sudo apt update

# Install DEB packages
sudo apt install python3-pip samba ffmpeg -y
sudo pip3 install pillow

# Configure smb.conf
sudo sed -i 's\;   interfaces = 127.0.0.0/8 eth0\interfaces = 127.0.0.0/8 eth0\g' /etc/samba/smb.conf
sudo sed -i 's\;   bind interfaces only = yes\bind interfaces only = yes\g' /etc/samba/smb.conf





# chown smb.conf to cat there our shares
sudo chown adminguide: /etc/samba/smb.conf
sudo cat >> /etc/samba/smb.conf << EOL
[mp4-to-gif-converter-outputs]
path = /opt/mp4-to-gif-converter/outputs
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes

[mp4-to-gif-converter-inputs]
path = /opt/mp4-to-gif-converter/inputs
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes

[mp4-to-gif-converter-backups]
path = /opt/mp4-to-gif-converter/backups
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes

[jpeg-to-webp-avif-converter-outputs]
path = /opt/jpeg-to-webp-avif-converter/outputs
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes

[jpeg-to-webp-avif-converter-inputs]
path = /opt/jpeg-to-webp-avif-converter/inputs
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes

[jpeg-to-webp-avif-converter-backups]
path = /opt/jpeg-to-webp-avif-converter/backups
public = yes
guest only = yes
writable = yes
force create mode = 0666
force directory mode = 0777
browseable = yes
EOL

# chown smb.conf back to root
sudo chown root: /etc/samba/smb.conf

# Create share folders
sudo mkdir -p -m=777 /opt/mp4-to-gif-converter/inputs
sudo mkdir -p -m=777 /opt/mp4-to-gif-converter/outputs
sudo mkdir -p -m=777 /opt/mp4-to-gif-converter/backups
sudo mkdir -p -m=777 /opt/jpeg-to-webp-avif-converter/inputs
sudo mkdir -p -m=777 /opt/jpeg-to-webp-avif-converter/outputs
sudo mkdir -p -m=777 /opt/jpeg-to-webp-avif-converter/backups
# Restart Samba daemon
sudo systemctl restart smbd

# Instal python3 packages
sudo pip3 install ffmpeg-python

# Create a systemd service unit file
sudo touch /etc/systemd/system/mp4-to-gif-converter.service
sudo touch /etc/systemd/system/jpeg-to-webp-avif-converter.service

# chown created a unit file to cat there a config
sudo chown adminguide: /etc/systemd/system/mp4-to-gif-converter.service
sudo chown adminguide: /etc/systemd/system/jpeg-to-webp-avif-converter.service

sudo cat >> /etc/systemd/system/mp4-to-gif-converter.service << EOL
[Unit]
Description=MP4 to GIF Converter by Artur Sagutdinov# The description of our service
After=multi-user.target # Target after which our service will start

[Service]
Type=simple
Restart=always
RestartSec=15
# Command to run our script
ExecStart=/usr/bin/python3 /opt/mp4-to-gif-converter/main.py 

[Install]
# Target where service symlink will be placed after enabling
WantedBy=multi-user.target
EOL

sudo cat >> /etc/systemd/system/jpeg-to-webp-avif-converter.service << EOL
[Unit]
Description=MP4 to GIF Converter by Artur Sagutdinov# The description of our service
After=multi-user.target # Target after which our service will start

[Service]
Type=simple
Restart=always
RestartSec=15
# Command to run our script
ExecStart=/usr/bin/python3 /opt/jpeg-to-webp-avif-converter/main.py 

[Install]
# Target where service symlink will be placed after enabling
WantedBy=multi-user.target
EOL

# chown updated unit file back to root
sudo chown root: /etc/systemd/system/mp4-to-gif-converter.service
sudo chown root: /etc/systemd/system/jpeg-to-webp-avif-converter.service

# enable mp4-to-gif-converter service autostart
sudo systemctl enable mp4-to-gif-converter jpeg-to-webp-avif-converter

# start mp4-to-gif-converter service
sudo systemctl start mp4-to-gif-converter jpeg-to-webp-avif-converter

# get mp4-to-gif-converter status
systemctl status mp4-to-gif-converter --no-pager
systemctl status jpeg-to-webp-avif-converter --no-pager

